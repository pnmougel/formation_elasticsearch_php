# formation_elasticsearch_php

### Prérequis

```
sudo apt install php7.2
sudo apt install php7.2-xml php7.2-curl php7.2-mbstring php7.2-zip

sudo apt install composer
```

Installation des dépendances

```
composer install
composer require annotations
composer require elasticsearch/elasticsearch
```

### Configuration

Dans `.env` modifier l'adresse vers le serveur elasticsearch en remplaçant 9200 par le port correspondant à l'instance.

```
ES_SERVER=http://formation.pnmougel.com:9200
```

# Démarrer le serveur

```
php bin/console server:run
```