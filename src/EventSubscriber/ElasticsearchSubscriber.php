<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

use Elasticsearch\ClientBuilder;

class ElasticsearchSubscriber implements EventSubscriberInterface
{
    public function __construct()
    {
        # $this->tokens = $tokens;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
//        $hosts = [getenv('ES_SERVER')];
//        $client = ClientBuilder::create()
//            ->setHosts($hosts)
//            ->build();

        // $controller = $event->getController();
        // getenv('ES_SERVER');
//        $event->getRequest()->attributes->set("a", "bla");
//        $event->getRequest()->attributes->set("esServer", $client);
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => 'onKernelController',
        );
    }
}