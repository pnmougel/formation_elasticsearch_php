<?php

namespace App\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Elasticsearch\ClientBuilder;

class ElasticsearchBaseController extends FOSRestController
{
    protected $esClient;

    public function __construct()
    {
        $hosts = [getenv('ES_SERVER')];
        $this->esClient = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();
    }
}