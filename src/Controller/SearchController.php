<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;

class SearchController extends ElasticsearchBaseController
{

    public function __construct(LoggerInterface $logger)
    {
        parent::__construct();
        $this->logger = $logger;
    }

    /**
     * @Route("/search/{query}", methods={"GET"})
     */
    public function search(LoggerInterface $logger, Request $request, $query)
    {

        $params = [
            'index' => 'entreprises',
            'body' => []
        ];
        $results = $this->esClient->search($params);

        return $this->json($results);
    }
}